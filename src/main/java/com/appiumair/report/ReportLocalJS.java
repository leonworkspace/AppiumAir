package com.appiumair.report;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 加载本地JS文件，解决JS文件路途遥远导致报告打开缓慢的问题
 */
public class ReportLocalJS {
    public static void localJS() {
        BufferedReader br = null;
        BufferedWriter bw = null;
        List<String> reportNameList = new ArrayList();
        reportNameList.add("index.html");
        reportNameList.add("sparkFail.html");
        reportNameList.add("sparkPass.html");
        for (int i = 0; i < reportNameList.size(); i++) {
            try {
                br = new BufferedReader(new FileReader("Spark/temp/" + reportNameList.get(i)));
                bw = new BufferedWriter(new FileWriter("Spark/" + reportNameList.get(i)));
                String line = null;
                while ((line = br.readLine()) != null) {
                    if (line.trim().equals("<script src=\"https://cdn.rawgit.com/extent-framework/extent-github-cdn/7cc78ce/spark/js/jsontree.js\"></script>")) {
                        line = "<script src='/appiumair/js/jsontree.js'></script>";
                    }
                    if (line.trim().equals("<title></title>")) {
                        line = "<title>AppiumAir-UI自动化测试报告</title>";
                    }
                    if (line.trim().equals("<ul class=\"nav-left\">")) {
                        line = "<ul class=\"nav-left\" style=\"margin-left: 170px;\">";
                    }
                    if (line.trim().equals("<div class=\"logo\" style=\"background-image: url('https://cdn.jsdelivr.net/gh/extent-framework/extent-github-cdn@b00a2d0486596e73dd7326beacf352c639623a0e/commons/img/logo.png')\"></div>")) {
                        line = "<div><img style=\"width: 200px;height: 55px;margin-left: -12px;\" src=\"https://gitee.com/songjianghu/AppiumAir/raw/master/src/main/resources/ico/logo-report.png\"></div>";
                    }
                    line = line.trim();
                    bw.write(line);
                    bw.newLine();
                    bw.flush();
                }
                bw.close();
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
